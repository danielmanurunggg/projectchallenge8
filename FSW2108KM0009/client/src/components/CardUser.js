import React from 'react'
import { Image, Button, Card, Container, Row, Col } from 'react-bootstrap'
import '../css/card.css'
// name, bio, avatar, level, created

const CardUser = (data)=> {
  const formatter = new Intl.DateTimeFormat("en-EN", {
    year: "numeric",
    month: "long",
    day: "2-digit"
  });
  return (
    <Container className='marginTop' >
      <Row className='gap-1'>
        {
          data.props.map((user) => (
            <Col lg sm>
            <Card bg='dark' key={user.id} className='card' style={{width:'16rem', height:'14rem'}}>
              <Card.Body>
              <Row className='mb-3'>
                <Col sm={3}>
                  <Image roundedCircle src={user.avatar} style={{width:'50px',height:'50px',objectFit:'cover'}}/>
                </Col>
                <Col >
                    <h1 className='name'>{user.name}</h1>
                    <h2 className='level'>{user.level}</h2>
                </Col>
              </Row>

                <p className='bio' style={{height:"30px"}}>{user.bio}</p>
                <p className='date'>since {formatter.format(new Date(user.createdAt))}</p>
                <Button style={{al:"left",width:"150px"}} variant='warning'>Fight</Button>
              </Card.Body>
            </Card>
            </Col>
          ))
        }
      </Row>
    </Container>
  )
}

export default CardUser