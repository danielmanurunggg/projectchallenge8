import React from 'react'
import {Navbar, Nav, Container} from "react-bootstrap";

const Dashboard = () => (
  <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed='top'>
    <Container>
    <Navbar.Brand href="/">Suit Games</Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className='ms-auto'>
        <Nav.Link style={{fontSize:"18px"}} href="/register">Register</Nav.Link>
        <Nav.Link style={{fontSize:"18px"}} href="/login">Login</Nav.Link>
      </Nav>
    </Navbar.Collapse>
    </Container>
  </Navbar>
)

export default Dashboard