import React from 'react'
import {Container, Button} from 'react-bootstrap'

const LevelButton = () => {
  return (
    <div style={{marginTop:"150px"}}>
      <Container>
        <h1 className="text-white mb-5">Choose your opponent</h1>
          <div className="text-center">
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Novice</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Class A</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Class B</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Class C</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Class D</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Candidate Master</Button>{' '}
              <Button style={{width:"13%",height:"60px"}} variant='secondary' type="button">Grand Master</Button>{' '}
          </div>
      </Container>
    </div>
  )
}

export default LevelButton
