import React from 'react'
import {Form, Button} from "react-bootstrap";
import "../css/form.css"

const Login = () => {
    return (
        <div className='flex'>
            <Form className="rounded p-5 p-sm-3">
                <div className='m-4'>
                    <h4 className='mb-4 fw-bold text-center'>LOGIN</h4>
                    <Form.Group className="mb-3" controlId="formBasicUsername">
                    <Form.Control
                        type='text'
                        placeholder="Insert your Username"
                    />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Control
                        type='password'
                        placeholder="Insert your Password"
                    />
                    </Form.Group>
                    <div className='d-grid mb-5'>
                        <Button variant="warning" className='text-white' type="submit" href="/">
                        Log In
                        </Button>
                    </div>
                </div>
            </Form>
        </div>
    )
}

export default Login