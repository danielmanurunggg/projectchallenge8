const DefaultLayout = ({ children }) => (
  <div>
    <header>
      <div>{children}</div>
    </header>
  </div>
);

export default DefaultLayout;
