import React from "react";
import DefaultLayout from "../layouts/background";
import Navbar from "../components/Dashboard";
import LevelButton from "../components/LevelButton";
import CardUser from "../components/CardUser";
import Database from "../opponents.json";

const Homepage = () => (
  <DefaultLayout>
    <Navbar />
    <LevelButton />
    <CardUser props={Database} />
  </DefaultLayout>
);

export default Homepage;
