import React from "react";
import DefaultLayout from "../layouts/background";
import CompForm from "../components/Login.js";

const Login = () => (
  <DefaultLayout>
    <CompForm />
  </DefaultLayout>
);

export default Login;
