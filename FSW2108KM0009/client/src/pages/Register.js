import React from "react";
import DefaultLayout from "../layouts/background";
import CompForm from "../components/Register";

const Register = () => (
  <DefaultLayout>
    <CompForm />
  </DefaultLayout>
);

export default Register;
