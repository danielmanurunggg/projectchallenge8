'use strict';

var utils = require('../utils/writer.js');
var Biodata = require('../service/BiodataService');

module.exports.getBios = function getBios (req, res, next) {
  Biodata.getBios()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateBiodata = function updateBiodata (req, res, next, body, userId) {
  Biodata.updateBiodata(body, userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
