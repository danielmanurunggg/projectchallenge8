'use strict';

var utils = require('../utils/writer.js');
var BiodataUser = require('../service/BiodataUserService');

module.exports.getBio = function getBio (req, res, next, userId) {
  BiodataUser.getBio(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
