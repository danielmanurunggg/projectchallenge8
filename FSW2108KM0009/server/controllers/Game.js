'use strict';

var utils = require('../utils/writer.js');
var Game = require('../service/GameService');

module.exports.gameUser = function gameUser (req, res, next, userId) {
  Game.gameUser(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getGame = function getGame (req, res, next) {
  Game.getGame()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.postOption = function postOption (req, res, next, body, roomId) {
  Game.postOption(body, roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
