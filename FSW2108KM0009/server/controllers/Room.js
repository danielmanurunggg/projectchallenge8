'use strict';

var utils = require('../utils/writer.js');
var Room = require('../service/RoomService');

module.exports.createRoom = function createRoom (req, res, next, body) {
  Room.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoom = function getRoom (req, res, next, isOpen) {
  Room.getRoom(isOpen)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.joinRoom = function joinRoom (req, res, next, roomId) {
  Room.joinRoom(roomId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
