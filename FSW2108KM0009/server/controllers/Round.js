'use strict';

var utils = require('../utils/writer.js');
var Round = require('../service/RoundService');

module.exports.getRound = function getRound (req, res, next) {
  Round.getRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.roundUser = function roundUser (req, res, next, userId) {
  Round.roundUser(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
