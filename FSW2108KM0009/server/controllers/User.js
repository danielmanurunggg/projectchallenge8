'use strict';

var utils = require('../utils/writer.js');
var User = require('../service/UserService');

module.exports.challengeOpponent = function challengeOpponent (req, res, next, userId) {
  User.challengeOpponent(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUsers = function getUsers (req, res, next, point, level) {
  User.getUsers(point, level)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
