'use strict';


/**
 * Get Bio
 * Get Bio by id
 *
 * userId Integer 
 * returns List
 **/
exports.getBio = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "Player" : {
    "phoneNumber" : "8998877665",
    "address" : "Depok Jawa Barat",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "avatarURL" : "https://www.google.com/",
    "fullName" : "Daniel Dwi Eryanto Manurung",
    "bio" : "I am a pro-player",
    "created_at" : "01-01-2021 12:00 +07:00"
  },
  "status" : "Get bio successfull"
}, {
  "Player" : {
    "phoneNumber" : "8998877665",
    "address" : "Depok Jawa Barat",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "avatarURL" : "https://www.google.com/",
    "fullName" : "Daniel Dwi Eryanto Manurung",
    "bio" : "I am a pro-player",
    "created_at" : "01-01-2021 12:00 +07:00"
  },
  "status" : "Get bio successfull"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

