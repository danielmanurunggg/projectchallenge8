'use strict';


/**
 * get game by user Id
 * get all game by user
 *
 * userId Integer 
 * returns inline_response_200_6
 **/
exports.gameUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "roundCount" : 3,
    "winner" : "player 2",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "isFinished" : true
  }, {
    "roundCount" : 3,
    "winner" : "player 2",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "isFinished" : true
  } ],
  "status" : "Get all game successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get All Game
 * get all game
 *
 * returns inline_response_200_6
 **/
exports.getGame = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "roundCount" : 3,
    "winner" : "player 2",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "isFinished" : true
  }, {
    "roundCount" : 3,
    "winner" : "player 2",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "isFinished" : true
  } ],
  "status" : "Get all game successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Player Make Choice
 * player choose the option
 *
 * body String 
 * roomId Integer 
 * returns inline_response_200_4
 **/
exports.postOption = function(body,roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "game" : {
    "player2Choose" : "waiting player 2",
    "player1Choose" : "Paper",
    "roomName" : "Room-12"
  },
  "status" : "Your choice has been saved"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

