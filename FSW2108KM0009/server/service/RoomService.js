'use strict';


/**
 * Create Room
 *
 * body Object 
 * returns inline_response_200_3
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : {
    "player1Id" : "1",
    "nameRoom" : "room-12",
    "player2Id" : "Waiting player 2",
    "created_at" : "01-01-2021 12:00 +07:00",
    "status" : "open"
  },
  "status" : "create room successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get Rooms
 * Get all Rooms
 *
 * isOpen Boolean  (optional)
 * returns inline_response_200_2
 **/
exports.getRoom = function(isOpen) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "gameId" : 1,
    "nameRoom" : "room-12",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "status" : "isOpen"
  }, {
    "gameId" : 1,
    "nameRoom" : "room-12",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "status" : "isOpen"
  } ],
  "status" : "Get all room successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Player Join Room
 * Player 2 join room
 *
 * roomId Integer 
 * returns inline_response_200_8
 **/
exports.joinRoom = function(roomId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "player1Id" : "1",
  "nameRoom" : "room-12",
  "player2Id" : "2",
  "status" : "Closed"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

