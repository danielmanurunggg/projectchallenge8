'use strict';


/**
 * Get All Round
 * Get All round
 *
 * returns inline_response_200_7
 **/
exports.getRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "winnerRound" : "Player 2",
    "finished_at" : "01-01-2021 12:00 +07:00",
    "player2Option" : "Scissor",
    "created_at" : "01-01-2021 12:00 +07:00",
    "player1Option" : "Paper"
  }, {
    "winnerRound" : "Player 2",
    "finished_at" : "01-01-2021 12:00 +07:00",
    "player2Option" : "Scissor",
    "created_at" : "01-01-2021 12:00 +07:00",
    "player1Option" : "Paper"
  } ],
  "status" : "Get all round successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get Round by user Id
 * Get All round by user Id
 *
 * userId Integer 
 * returns inline_response_200_7
 **/
exports.roundUser = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "Room" : [ {
    "winnerRound" : "Player 2",
    "finished_at" : "01-01-2021 12:00 +07:00",
    "player2Option" : "Scissor",
    "created_at" : "01-01-2021 12:00 +07:00",
    "player1Option" : "Paper"
  }, {
    "winnerRound" : "Player 2",
    "finished_at" : "01-01-2021 12:00 +07:00",
    "player2Option" : "Scissor",
    "created_at" : "01-01-2021 12:00 +07:00",
    "player1Option" : "Paper"
  } ],
  "status" : "Get all round successfull"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

