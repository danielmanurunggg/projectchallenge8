'use strict';


/**
 * Challenge other player
 * Challenge other player
 *
 * userId Integer 
 * returns inline_response_200_5
 **/
exports.challengeOpponent = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "gameId" : 1,
    "nameRoom" : "room-12",
    "created_at" : "01-01-2021 12:00 +07:00",
    "status" : "waiting for opponent"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all users
 * Get all users from database
 *
 * point Integer  (optional)
 * level String  (optional)
 * returns List
 **/
exports.getUsers = function(point,level) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "Player" : {
    "password" : "password",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "id" : 1,
    "username" : "danielmanurunggg"
  },
  "status" : "Successfull"
}, {
  "Player" : {
    "password" : "password",
    "updated_at" : "01-01-2021 12:00 +07:00",
    "created_at" : "01-01-2021 12:00 +07:00",
    "id" : 1,
    "username" : "danielmanurunggg"
  },
  "status" : "Successfull"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

